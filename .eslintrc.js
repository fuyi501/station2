module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "@vue/prettier"],
  rules: {
    // "no-console": process.env.NODE_ENV === "production" ? "off" : "off",
    // "no-debugger": process.env.NODE_ENV === "production" ? "off" : "off",
    // "vue/no-parsing-error": [2, { "x-invalid-end-tag": false }]
    "no-console": 2,
    "no-debugger": 2,
    "vue/no-parsing-error": [2, { "x-invalid-end-tag": false }]
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};
