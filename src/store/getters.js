/*
包含多个基于 state 的 getter 计算属性的对象
*/
import dayjs from 'dayjs'

export default {
  todayXAxisDataList () { //  x 轴坐标，一天的时间 01 ～ 24
    let todayXAxis = []
    for(let i=0;i<=24;i++){
      todayXAxis.push(i < 10 ? '0' + i + ':00' : i + ':00')
    }
    return todayXAxis
  },
  weekXAxisDataList () { // 近七天的时间
    let weekXAxis = []
    for (let i = 7; i > 0; i--) {
      weekXAxis.push(dayjs().subtract(i, "day").startOf("day").format("MM-DD"))
    }
    return weekXAxis
  },
  monthXAxisDataList () { // 近 30 天的时间
    let monthXAxis = []
    for (let i = 30; i > 0; i--) {
      monthXAxis.push(dayjs().subtract(i, "day").startOf("day").format("MM-DD"))
    }
    return monthXAxis
  },
  dayWeekMonthStartEndTime () {
    let monthStartTime = dayjs().subtract(30, "day").startOf("day").format("YYYY-MM-DD HH:mm:ss")
    let weekStartTime = dayjs().subtract(7, "day").startOf("day").format("YYYY-MM-DD HH:mm:ss")
    let todayStartTime = dayjs().startOf("day").format("YYYY-MM-DD HH:mm:ss")
    let todayEndTime = dayjs().add(1, 'hour').format("YYYY-MM-DD HH:mm:ss")
    let todayRealTime = dayjs().add(1, 'hour').format("YYYY-MM-DD HH:mm:ss")
    return {todayStartTime, todayEndTime, todayRealTime, weekStartTime, monthStartTime}
  },
  // 进店服务率，服务人数/总人数
  serviceEnterRate(state) {
    if(state.servicePersonsData.length > 0 && state.enterPersonsData.length > 0) {
      return state.servicePersonsData.map((ele, index) => {
        let enterPersons = state.enterPersonsData[index]===0?ele:state.enterPersonsData[index]
        let serviceRate = ele/enterPersons
        console.log(serviceRate)
        serviceRate = isNaN(serviceRate) ? 0:(serviceRate*100)
        serviceRate = serviceRate>100 ? 100:(serviceRate).toFixed(2)
        return serviceRate
      })
    }
  }
}
