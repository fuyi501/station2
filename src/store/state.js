/*
状态对象
*/
export default {
  // 主页数据
  fourItemScoreData: [ // 四项评分数据
    { name: "安全", en: "safety", value: 0, score: 85 },
    { name: "效率", en: "efficiency", value: 0, score: 88 },
    { name: "管理", en: "management", value: 0, score: 81 },
    { name: "服务", en: "service", value: 0, score: 77 }
  ],
  overallScoreTrendData: [], // 分公司综合评分变化趋势数据
  fourItemScoreTrendData: [], // 分公司四项评分变化趋势数据

  // 站点分析页面
  selectedStationData: {
    station: "fengchengshilu",
    dateTime: "today"
  },
  // 站点分析页面 左侧数据
  countData: [
    { title: "服务总人次", value: 0 },
    { title: "进店总人次", value: 0 },
    { title: "收银服务时间", value: "0 分钟" },
    { title: "便利店违规率", value: "0 %" }
  ],
  servicePersonsData: [], // 服务人次
  enterPersonsData: [], // 进店人次
  avgServiceTimeData: [], // 平均服务时间
  managerOnlineRateData: [], // 站长在站率数据

  // 站点 评分数据
  stationScoreData: {
    overall: 0, // 总评分
    safety: 0, // 安全评分
    efficiency: 0, // 效率评分
    management: 0, // 管理评分
    service: 0, // 服务评分
    checkout: 0, // 便利店
    refuel: 0, // 加油区
    safebox: 0, // 财务室
    unload: 0 // 卸油口
  },
  // 站点 历史评分数据
  stationScoreListData: {
    overall: [], // 总评分
    safety: [], // 安全评分
    efficiency: [], // 效率评分
    management: [], // 管理评分
    service: [], // 服务评分
    checkout: [], // 便利店
    refuel: [], // 加油区
    safebox: [], // 财务室
    unload: [] // 卸油口
  },
  // 加油区服务数据
  refuelServiceData: {
    serviceCarsSum: 0, // 服务总车辆数
    serviceAllTime: 0, // 车辆服务总时间，总值
    serviceArgTime: 0, // 车辆服务平均时间，平均值
    serviceCarsData: [], // 服务车辆数
    avgServiceTimeData: [], // 平均车辆服务时间
  },
  // 违规事件统计
  violateEventData: {
    checkoutSum: 0, // 便利店违规事件总数
    refuelSum: 0, // 加油区违规事件总数
    safeboxSum: 0, // 保险柜违规事件总数
    unloadSum: 0, // 卸油口违规事件总数
    checkoutVE: [], // 便利店违规事件
    refuelVE: [], // 加油区违规事件
    safeboxVE: [], // 保险柜违规事件
    unloadVE: [] // 卸油口违规事件
  },
  stationCenterCountData: [
    { title: "卸油次数", value: "0 次" },
    { title: "加油区违规率", value: "0 %" },
    { title: "保险柜打开次数", value: "0 次" },
    { title: "钱箱打开次数", value: "0 次" }
  ],
  // 事件表格数据
  eventTableData: [],
  // 拥堵指数
  jamIndexData: [],

  refuelServiceName: [], // 加油岛名称
  refuelServiceTime: [], // 加油岛对应服务时间

  monitorData: {
    station: 'fengchengshilu',
    videoInfoList: '', // 视频流信息
    isLoadPlayer: false, // 是否打开视频流播放，false 为不加载，true 为显示视频流播放页面
    reqPlayerInfo: ''
  },
  videoPlayerState: {
    getStreamUrlState: 200, // 获取视频流状态， 200 获取成功，404 获取失败
  },
  stationVideoStreamUrl: [], // 站点视频流地址

  monitorEventsTableListData: [],

  // 报警事件查询的数据
  eventsQueryFormData: {
    subCompany: "",
    station: "",
    category: "",
    action: "",
    eventLevel: "",
    timeRange: ""
  }
}