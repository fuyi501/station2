/*
直接更新 state 的多个方法的对象
*/
export default {
  // 获取站点视频流地址
  GETSTATIONVIDEOSTREAMURL (state, {videoList}) {
    state.stationVideoStreamUrl = videoList
    state.monitorData.videoInfoList = videoList
    state.getStreamUrlState = 200
  },
  // 更新视频流获取状态到 404
  CHANGEGETVEDIOSTREAMSTATE404 (state) {
    state.videoPlayerState.getStreamUrlState = 404
  },
  // 更新视频流获取状态到 200
  CHANGEGETVEDIOSTREAMSTATE200 (state) {
    state.videoPlayerState.getStreamUrlState = 200
  },
  // 更新 站点视频流 推流状态
  UPDATESTATIONVIDEOSTREAMSTATE (state, {videoList}) {
    if(state.stationVideoStreamUrl.length === 0) return
    videoList.forEach((ele, index) => {
      if(ele.channel ===  state.stationVideoStreamUrl[index].channel) {
        state.stationVideoStreamUrl[index].pushing = ele.pushing
      }
    })
  },
  // 加载视频流实时播放页面
  ONLOADVIDEOSTREAMPLAYER (state, {reqPlayerInfo}) {
    state.monitorData.isLoadPlayer = true // false 为不加载，true 为显示视频流播放页面
    state.monitorData.reqPlayerInfo = reqPlayerInfo
  },
  // 关闭视频流详情页，打开视频流网格页面
  CLOSEDETAILSLOADVIDEOGRIDPAGES (state) {
    state.monitorData.isLoadPlayer = false
  },
  GETMONITOREVENTSTABLELIST (state, { monitorEventsData }) {
    state.monitorEventsTableListData = monitorEventsData
  },
  // 获取四项评分
  GETNOWFOURSCORE (state, {fourScore}) {
    state.fourItemScoreData = fourScore
  },
  // 获取综合评分变化趋势
  GETOVERALLSCORETRENDDATA (state, {overallScoreData, fourItemScoreData}) {
    state.overallScoreTrendData = overallScoreData
    state.fourItemScoreTrendData = fourItemScoreData
  },

  // 更新选择的站点数据
  UPDATESELECTEDSTATIONDATA (state, {selectedData}) {
    state.selectedStationData = selectedData
  },
  // 初始化所有状态数据
  INITALLSTATEDATA (state) {
    state.servicePersonsData = [] // 服务人次
    state.enterPersonsData = [] // 进店人次
    state.avgServiceTimeData = [] // 平均服务时间
    state.managerOnlineRateData = [] // 站长在站率数据

    // 违规事件统计
    state.violateEventData.checkoutVE = []
    state.violateEventData.refuelVE = []
    state.violateEventData.safeboxVE = []
    state.violateEventData.unloadVE = []

    // 站点 历史评分数据
    state.stationScoreListData = {
      overall: [], // 总评分
      safety: [], // 安全评分
      efficiency: [], // 效率评分
      management: [], // 管理评分
      service: [], // 服务评分
      checkout: [], // 便利店
      refuel: [], // 加油区
      safebox: [], // 财务室
      unload: [] // 卸油口
    }

    state.jamIndexData = [], // 拥堵指数
    // 加油区服务数据
    state.refuelServiceData.serviceCarsData = []
    state.refuelServiceData.avgServiceTimeData = []
    state.refuelServiceName = [], // 加油岛名称
    state.refuelServiceTime = [] // 加油岛对应服务时间
  },
  // 获取加油区或商超区 服务人数和服务时间
  GETSERVICESTATISTIC (state, { getServiceData }) {

    state.countData[0].value = getServiceData.servicePersonsSum
    state.countData[2].value = getServiceData.serviceArgTime + '分钟'
    state.servicePersonsData = getServiceData.servicePersonsData
    state.avgServiceTimeData = getServiceData.avgServiceTimeData
  },
  // 获取商超进店人次
  GETCUSTOMERENTER (state, {getCustomerEnterData}) {
    state.countData[1].value = getCustomerEnterData.enterPersonsSum
    state.enterPersonsData = getCustomerEnterData.enterPersonsData
  },
  // 获取 便利店实时违规率
  GETNOWVIOLATERATE (state, {getViolateRateData}) {
    state.countData[3].value = getViolateRateData + '%'
  },
  // 获取 便利店 历史违规率
  GETCHECHOUTHISTORYVIOLATERATE (state, {getHistoryViolateRateData}) {
    state.countData[3].value = getHistoryViolateRateData + '%'
  },
  // 获取 加油区 历史违规率
  GETREFUELHISTORYVIOLATERATE (state, {getHistoryViolateRateData}) {
    state.stationCenterCountData[1].value = getHistoryViolateRateData + '%'
  },
  // 获取站长在站率, 没有实时数据，只有周数据和月数据
  GETMANAGERONLINERATE (state, {getManagerOnlineRateData}) {
    state.managerOnlineRateData = getManagerOnlineRateData.managerOnlineRateData
  },
  // 获取 实时综合指数评分
  GETCOMPOSITEINDEXNOW (state, {scoreData}) {
    state.stationScoreData = scoreData
  },
  // 获取加油区 服务车辆数和服务时间
  GETREFUELSERVICESTATISTIC (state, { getServiceData }) {
    state.refuelServiceData = getServiceData
  },
  // 获取加油站违规事件数据
  GETVIOLATEEVENTDATA (state, { violateData }) {
    state.violateEventData = violateData
  },
  // 获取卸油次数，保险柜打开次数，钱箱打开次数
  GETSTATIONCENTERCOUNTDATA (state, { countData }) {
    state.stationCenterCountData[0].value = countData.unloadCount + '次'
    state.stationCenterCountData[2].value = countData.safeboxCount + '次'
    state.stationCenterCountData[3].value = countData.moneyboxCount + '次'
  },
  // 获取 加油区实时违规率
  GETREFUELNOWVIOLATERATE (state, { refuelViolateRateData }) {
    state.stationCenterCountData[1].value = refuelViolateRateData + '%'
  },
  // 获取 事件表格数据
  GETEVENTSTABLEDATA (state, {eventTableData}) {
    state.eventTableData = eventTableData
  },
  // 获取 实时拥堵指数数据
  GETJAMINDEXDATA (state, {jamIndex}) {
    state.jamIndexData = jamIndex
  },
  // 获取 实时加油岛服务时间
  GETNOWREFUELSERVICETIMEDATA (state, {refuelServiceName, refuelServiceTime}) {
    state.refuelServiceName = refuelServiceName
    state.refuelServiceTime = refuelServiceTime
  },
  // 获取 加油岛历史服务时间
  GETHISTORYREFUELSERVICETIMEDATA (state, {refuelServiceIndexName, refuelServiceIndexTime}) {
    state.refuelServiceName = refuelServiceIndexName
    state.refuelServiceTime = refuelServiceIndexTime
  },
  // 获取 站点历史综合评分指标
  GETSTATIONSCORETRENDDATA (state, { stationScoreListData, stationScoreData}) {
    state.stationScoreListData = stationScoreListData
    state.stationScoreData = stationScoreData
  },
  UPDATEEVENTSQUERYFORMDATA (state, {newFormData}) {
    state.eventsQueryFormData = newFormData
  }
}
