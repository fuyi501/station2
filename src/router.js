import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home/Home.vue";
import Monitor from "./views/Monitor/Monitor.vue";
import Monitor2 from "./views/Monitor2";
import Events from "./views/Events/Events.vue";
import Events2 from "./views/Events2";
import StationAnalysis from "./views/StationAnalysis";
import TabContent from "./views/StationAnalysis/tabContent.vue"
import SubCompanyAnalysis from "./views/SubCompanyAnalysis";

import Home2 from "./views/Home2";
import Test from "./views/test/test.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "station-analysis",
      component: StationAnalysis
    },
    {
      path: "/monitor",
      name: "monitor",
      component: Monitor
    },
    {
      path: "/monitor2",
      name: "monitor2",
      component: Monitor2
    },
    {
      path: "/sub-company-analysis/:companyName/:dateTime",
      name: "sub-company-analysis",
      component: SubCompanyAnalysis
    },
    {
      path: "/station-analysis",
      name: "station-analysis",
      component: StationAnalysis,
      children: [
        {
          path: 'station/:station/:dateTime',
          name: 'station',
          component: TabContent
        }
      ]
    },
    {
      path: "/events",
      name: "events",
      component: Events
    },
    {
      path: "/events2",
      name: "events2",
      component: Events2
    },
    {
      path: "/test",
      name: "test",
      component: Test
    },
    {
      path: "/home2",
      name: "home2",
      component: Home2
    },
  ]
});
