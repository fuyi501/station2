/* eslint-disable prettier/prettier */
export default {
  // 分公司列表
  subCompanyList: [
    { value: "xian", label: "西安分公司", id: "49" },
    { value: "baoji", label: "宝鸡分公司", id: "1" },
    { value: "yanan", label: "延安分公司", id: "2" },
    { value: "tongchuan", label: "铜川分公司", id: "3" },
    { value: "weinan", label: "渭南分公司", id: "4" },
    { value: "xianyang", label: "咸阳分公司", id: "5" },
    { value: "yulin", label: "榆林分公司", id: "6" },
    { value: "hanzhong", label: "汉中分公司", id: "7" },
    { value: "ankang", label: "安康分公司", id: "8" },
    { value: "shangluo", label: "商洛分公司", id: "9" }
  ],
  subCompanyData: {
    xian: "西安分公司",
    baoji: "宝鸡分公司",
    yanan: "延安分公司",
    tongchuan: "铜川分公司",
    weinan: "渭南分公司",
    xianyang: "咸阳分公司",
    yulin: "榆林分公司",
    hanzhong: "汉中分公司",
    ankang: "安康分公司",
    shangluo: "商洛分公司"
  },
  // 站点列表
  stationList: [
    { value: "all", subCompany: "49", label: "全部" },
    { value: "xiwan", subCompany: "xian", label: "西万路" },
    { value: "ximen", subCompany: "xian", label: "西门" },
    { value: "mingdemen", subCompany: "xian", label: "明德门" },
    { value: "zhuhong", subCompany: "xian", label: "朱宏路" },
    { value: "fengchengshilu", subCompany: "xian", label: "凤城十路" },
    { value: "caotan", subCompany: "xian", label: "草滩路" },
    // { value: "unknown", subCompany: "baoji", label: "宝鸡站点" },
    // { value: "unknown1", subCompany: "yanan", label: "延安站点" },
    // { value: "unknown2", subCompany: "tongchuan", label: "铜川站点" },
    // { value: "unknown3", subCompany: "weinan", label: "渭南站点" },
    // { value: "unknown4", subCompany: "xianyang", label: "咸阳站点" },
    // { value: "unknown5", subCompany: "yulin", label: "榆林站点" },
    // { value: "unknown6", subCompany: "hanzhong", label: "汉中站点" },
    // { value: "unknown7", subCompany: "ankang", label: "安康站点" },
    // { value: "unknown8", subCompany: "shangluo", label: "商洛站点" }
  ],
  // 站点对应关系
  stationData: {
    xiwan: "西万",
    ximen: "西门",
    mingdemen: "明德门",
    zhuhong: "朱宏路",
    fengchengshilu: "凤城十路",
    fengshi: "凤城十路",
    caotan: "草滩",
    unknown: "未知站点"
  },
  // 场景列表
  categoryList: [
    { value: "all", label: "全部" },
    { value: "checkout", label: "便利店" },
    { value: "refuel_side", label: "加油现场" },
    { value: "safebox", label: "财务室" },
    { value: "unload", label: "卸油现场" }
  ],
  categoryData: {
    unload: "卸油口",
    refuel_side: "加油区",
    checkout: "便利店",
    safebox: "财务室",
    gate: "大门",
    refuel_overview: "加油区全景",
    switching_room: "配电房"
  },
  // 事件列表
  actionList: [
    // 卸油现场
    { value: "0", category: "unload", label: "全部", status: "1" },
    { value: "1", category: "unload", label: "卸油口开启", status: "1" },
    { value: "2", category: "unload", label: "接油管操作", status: "1" },
    { value: "3", category: "unload", label: "油罐车到达", status: "1" },
    { value: "4", category: "unload", label: "油罐车离开", status: "1" },
    { value: "5", category: "unload", label: "卸油口关闭", status: "1" },
    { value: "6", category: "unload", label: "断开静电夹", status: "1" },
    { value: "7", category: "unload", label: "连接静电夹", status: "1" },
    { value: "8", category: "unload", label: "员工离开", status: "1" },
    { value: "9", category: "unload", label: "油井巡检", status: "1" },
    // { value: "10", category: "unload", label: "水溶法检测操作", status: "1" },
    { value: "11", category: "unload", label: "卸油口巡检", status: "1" },
    { value: "12", category: "unload", label: "已放置消防器材", status: "1" },
    { value: "13", category: "unload", label: "非法入侵", status: "1" },
    // 加油现场
    { value: "0", category: "refuel_side", label: "全部", status: "1" },
    { value: "1", category: "refuel_side", label: "服务不规范", status: "1" },
    { value: "2", category: "refuel_side", label: "油井巡检", status: "1" },
    { value: "3", category: "refuel_side", label: "巡检", status: "1" },
    { value: "4", category: "refuel_side", label: "未引导", status: "1" },
    { value: "5", category: "refuel_side", label: "未回零", status: "1" },
    { value: "6", category: "refuel_side", label: "胶管乱摆放", status: "1" },
    // 便利店
    { value: "0", category: "checkout", label: "全部", status: "1" },
    { value: "1", category: "checkout", label: "单手接递", status: "1" },
    { value: "2", category: "checkout", label: "双手接递", status: "0" },
    { value: "3", category: "checkout", label: "服务超时", status: "1" },
    { value: "4", category: "checkout", label: "盗刷POS机", status: "1" },
    // 财务室
    { value: "0", category: "safebox", label: "全部", status: "1" },
    { value: "1", category: "safebox", label: "开启保险柜", status: "1" },
    { value: "2", category: "safebox", label: "关闭保险柜", status: "0" },
    { value: "3", category: "safebox", label: "开启钱箱", status: "1" },

    // 全部
    { value: "0", category: "all", label: "全部", status: "1" },
    { value: "1", category: "all", label: "卸油口开启", status: "1" },
    { value: "2", category: "all", label: "接油管操作", status: "1" },
    { value: "3", category: "all", label: "油罐车到达", status: "1" },
    { value: "4", category: "all", label: "油罐车离开", status: "1" },
    { value: "5", category: "all", label: "卸油口关闭", status: "1" },
    { value: "6", category: "all", label: "断开静电夹", status: "1" },
    { value: "7", category: "all", label: "连接静电夹", status: "1" },
    { value: "8", category: "all", label: "员工离开", status: "1" },
    { value: "9", category: "all", label: "油井巡检", status: "1" },
    { value: "10", category: "all", label: "卸油口巡检", status: "1" },
    { value: "11", category: "all", label: "已放置消防器材", status: "1" },
    { value: "12", category: "all", label: "非法入侵", status: "1" },
    { value: "13", category: "all", label: "服务不规范", status: "1" },
    { value: "14", category: "all", label: "油井巡检", status: "1" },
    { value: "15", category: "all", label: "巡检", status: "1" },
    { value: "16", category: "all", label: "未引导", status: "1" },
    { value: "17", category: "all", label: "未回零", status: "1" },
    { value: "18", category: "all", label: "胶管乱摆放", status: "1" },
    { value: "20", category: "all", label: "单手接递", status: "1" },
    { value: "21", category: "all", label: "双手接递", status: "0" },
    { value: "22", category: "all", label: "服务超时", status: "1" },
    { value: "23", category: "all", label: "盗刷POS机", status: "1" },
    { value: "25", category: "all", label: "开启保险柜", status: "1" },
    { value: "26", category: "all", label: "关闭保险柜", status: "0" },
    { value: "27", category: "all", label: "开启钱箱", status: "1" }
  ],
  unloadActionData: {
    0: "全部",
    1: "卸油口开启",
    2: "接油管操作",
    3: "油罐车到达",
    4: "油罐车离开",
    5: "卸油口关闭",
    6: "断开静电夹",
    7: "连接静电夹",
    8: "员工离开",
    9: "油井巡检",
    // 10: "水溶法检测操作",
    11: "卸油口巡检",
    12: "已放置消防器材",
    13: "非法入侵"
  },
  oilActionData: {
    0: "全部",
    1: "服务不规范",
    2: "油井巡检",
    3: "巡检",
    4: "未引导",
    5: "未回零",
    6: "胶管乱摆放"
  },
  checkoutActionData: {
    0: "全部",
    1: "单手接递",
    2: "双手接递",
    3: "服务超时",
    4: "盗刷POS机"
  },
  safeBoxActionData: {
    0: "全部",
    1: "开启保险柜",
    2: "关闭保险柜",
    3: "开启钱箱"
  },
  // 操作行为状态
  // 只有合规和违规两种，但是违规分为 1，2，3 三个等级
  levelList: [
    { value: "4", label: "全部" },
    { value: "0", label: "合规" },
    { value: "1", label: "轻度违规" },
    { value: "2", label: "中度违规" },
    { value: "3", label: "重度违规" }
  ],
  levelData: {
    4: "全部",
    0: "合规",
    1: "轻度违规",
    2: "中度违规",
    3: "重度违规"
  },
  levelStatusList: [
    { value: "0", label: "合规" },
    { value: "1", label: "违规" }
  ],
  levelStatusData: {
    0: "合规",
    1: "违规"
  },
  channelList: [
    // 西万
    { value: "1", station: "xiwan", category: "checkout", label: "西万摄像头1" },
    { value: "2", station: "xiwan", category: "gate", label: "西万摄像头2" },
    { value: "3", station: "xiwan", category: "refuel_overview", label: "西万摄像头3" },
    { value: "4", station: "xiwan", category: "refuel_side", label: "西万摄像头4" },
    { value: "5", station: "xiwan", category: "refuel_side", label: "西万摄像头5" },
    { value: "6", station: "xiwan", category: "refuel_side", label: "西万摄像头6" },
    { value: "7", station: "xiwan", category: "refuel_side", label: "西万摄像头7" },
    { value: "8", station: "xiwan", category: "safebox", label: "西万摄像头8" },
    { value: "9", station: "xiwan", category: "unload", label: "西门摄像头9" },
    { value: "10", station: "xiwan", category: "refuel_side", label: "西门摄像头10" },
    // 西门
    { value: "1", station: "ximen", category: "checkout", label: "西门摄像头1" },
    { value: "2", station: "ximen", category: "gate", label: "西门摄像头2" },
    { value: "3", station: "ximen", category: "refuel_overview", label: "西门摄像头3" },
    { value: "4", station: "ximen", category: "refuel_side", label: "西门摄像头4" },
    { value: "5", station: "ximen", category: "refuel_side", label: "西门摄像头5" },
    { value: "6", station: "ximen", category: "refuel_side", label: "西门摄像头6" },
    { value: "7", station: "ximen", category: "refuel_side", label: "西门摄像头7" },
    { value: "8", station: "ximen", category: "safebox", label: "西门摄像头8" },
    { value: "9", station: "ximen", category: "unload", label: "西门摄像头9" },
    { value: "10", station: "ximen", category: "refuel_overview", label: "西门摄像头10" },
    // 明德门
    { value: "1", station: "mingdemen", category: "checkout", label: "明德门摄像头1" },
    { value: "2", station: "mingdemen", category: "gate", label: "明德门摄像头2" },
    // { value: "3", station: "mingdemen", category: "refuel_overview", label: "明德门摄像头3" },
    // { value: "4", station: "mingdemen", category: "refuel_side", label: "明德门摄像头4" },
    // { value: "5", station: "mingdemen", category: "refuel_side", label: "明德门摄像头5" },
    // { value: "6", station: "mingdemen", category: "refuel_side", label: "明德门摄像头6" },
    // { value: "7", station: "mingdemen", category: "refuel_side", label: "明德门摄像头7" },
    { value: "8", station: "mingdemen", category: "safebox", label: "明德门摄像头8" },
    { value: "9", station: "mingdemen", category: "unload", label: "明德门摄像头9" },
    // { value: "10", station: "mingdemen", category: "refuel_side", label: "明德门摄像头10" },
    // 朱宏路
    { value: "1", station: "zhuhong", category: "checkout", label: "朱宏路摄像头1" },
    { value: "2", station: "zhuhong", category: "gate", label: "朱宏路摄像头2" },
    { value: "3", station: "zhuhong", category: "refuel_overview", label: "朱宏路摄像头3" },
    { value: "4", station: "zhuhong", category: "refuel_side", label: "朱宏路摄像头4" },
    { value: "5", station: "zhuhong", category: "refuel_side", label: "朱宏路摄像头5" },
    { value: "6", station: "zhuhong", category: "refuel_side", label: "朱宏路摄像头6" },
    { value: "7", station: "zhuhong", category: "refuel_side", label: "朱宏路摄像头7" },
    // { value: "8", station: "zhuhong", category: "safebox", label: "朱宏路摄像头8" },
    { value: "9", station: "zhuhong", category: "unload", label: "朱宏路摄像头9" },
    { value: "10", station: "zhuhong", category: "unload", label: "朱宏路摄像头10" },
    { value: "11", station: "zhuhong", category: "unload", label: "朱宏路摄像头11" },
    { value: "12", station: "zhuhong", category: "unload", label: "朱宏路摄像头12" },
    { value: "13", station: "zhuhong", category: "unload", label: "朱宏路摄像头13" },
    // 凤城十路
    { value: "1", station: "fengchengshilu", category: "checkout", label: "凤城十路摄像头1" },
    { value: "2", station: "fengchengshilu", category: "gate", label: "凤城十路摄像头2" },
    { value: "3", station: "fengchengshilu", category: "refuel_overview", label: "凤城十路摄像头3" },
    { value: "4", station: "fengchengshilu", category: "refuel_side", label: "凤城十路摄像头4" },
    { value: "5", station: "fengchengshilu", category: "refuel_side", label: "凤城十路摄像头5" },
    { value: "6", station: "fengchengshilu", category: "refuel_side", label: "凤城十路摄像头6" },
    { value: "7", station: "fengchengshilu", category: "refuel_side", label: "凤城十路摄像头7" },
    { value: "8", station: "fengchengshilu", category: "safebox", label: "凤城十路摄像头8" },
    { value: "9", station: "fengchengshilu", category: "unload", label: "凤城十路摄像头9" },
    { value: "10", station: "fengchengshilu", category: "switching_room", label: "凤城十路摄像头10" },
    { value: "11", station: "fengchengshilu", category: "refuel_side", label: "凤城十路摄像头11" },
    { value: "12", station: "fengchengshilu", category: "refuel_side", label: "凤城十路摄像头12" },
    // { value: "13", station: "fengchengshilu", category: "unload", label: "凤城十路摄像头13" }
    // 草滩
    // { value: "1", station: "caotan", category: "checkout", label: "草滩摄像头1" },
    // { value: "2", station: "caotan", category: "gate", label: "草滩摄像头2" },
    // { value: "3", station: "caotan", category: "refuel_overview", label: "草滩摄像头3" },
    // { value: "4", station: "caotan", category: "refuel_side", label: "草滩摄像头4" },
    // { value: "5", station: "caotan", category: "refuel_side", label: "草滩摄像头5" },
    // { value: "6", station: "caotan", category: "refuel_side", label: "草滩摄像头6" },
    // { value: "7", station: "caotan", category: "refuel_side", label: "草滩摄像头7" },
    // { value: "8", station: "caotan", category: "safebox", label: "草滩摄像头8" },
    // { value: "9", station: "caotan", category: "unload", label: "草滩摄像头9" }
  ]
};
