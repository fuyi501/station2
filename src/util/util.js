import dayjs from "dayjs";
/* 获取一个月的日期和当前日期
 * param formatStr: 模板字符串 如"YYYY-MM-DD HH:mm:ss"
 * return: 如{startTime:'2019-05-01 12:00:00',endTime:'2019-06-01 12:00:00'}
 */
export const getLastMonthDate = function(formatStr = "YYYY-MM-DD 00:00:00") {
  let startTime = dayjs().set("month", dayjs().month() - 1).format(formatStr);
  let endTime = dayjs().format(formatStr);
  return { startTime, endTime };
};
/* 获取一周前的日期和当前日期
 * param formatStr: 模板字符串 如"YYYY-MM-DD HH:mm:ss"
 * return: 如{startTime:'2019-05-01 12:00:00',endTime:'2019-05-07 12:00:00'}
 */
export const getLastWeekDate = function(formatStr = "YYYY-MM-DD 00:00:00") {
  let startTime = dayjs().subtract(7, "day").startOf("day").format(formatStr);
  let endTime = dayjs().startOf("day").format(formatStr);
  return { startTime, endTime };
};
/* 获取 x 轴类目
 * param diffDays: 起始日期与当前日期相隔的天数 如表示最近一周，则传入7
 * param formatStr: 模板字符串 如"MM-DD"
 * return: 返回相隔的日期 如["05-01","05-02","05-03","05-04","05-05","05-06","05-07"]
 */
export const formatXAxisData = function(diffDays, formatStr) {
  let diffDate = [];
  for (let i = diffDays; i > 0; i--) {
    diffDate.push(dayjs().subtract(i, "day").format(formatStr));
  }
  return diffDate;
};
/* 获取时间序列，对应返回的数据的 key 值
 * param diff: 起始日期与当前日期相隔的差值 ,和 type 一起使用，type 为 hour 或者 day
 * param formatStr: 模板字符串 如"YYYY-MM-DD HH:mm:ss"
 * return: 返回相隔的日期 如["05-01","05-02","05-03","05-04","05-05","05-06","05-07"]
 */
export const getTimeListData = function() {
  // 根据 x 轴的数据遍历数据
  let timeList = [];
  let axisList = [];
  let st = dayjs().startOf("day").format("YYYY-MM-DD HH:mm:ss");
  let et = dayjs().format("YYYY-MM-DD HH:mm:ss");
  let diff = dayjs(et).diff(dayjs(st), "hour");

  for (let i = diff; i > 0; i--) {
    timeList.push(dayjs().subtract(i, "hour").startOf("hour").format("YYYY-MM-DD HH:mm:ss"));
    axisList.push(dayjs().subtract(i, "hour").startOf("hour").format("HH:mm"));
  }

  return { timeList, axisList };
};
/* 通过value获取id
 * param list: 数组，结构参考constData的subCompanyList，需保证list中value唯一
 * param value: 值 如"xian"
 * return: id 如 "49"
 */
export const getIdByValue = function(list, value) {
  let arr = list.filter((item, index, list) => {
    return item.value == value;
  });
  return arr[0].id;
};
/* 按照数组对象中某个属性值进行排序
 * param list: 数组
 * param attr: 属性值 如"total"
 * param rev: bool值,默认为false 如true表示升序,false表示降序
 * return
 */
export const sortByAttr = function(list, attr, rev = false) {
  try {
    let r = rev ? 1 : -1;
    return list.sort((item1, item2) => {
      return (item1[attr] - item2[attr]) * r;
    });
  } catch (e) {
    throw "参数需为Array类型";
  }
};

export const changeSecondsToMinutes = function(seconds) {
  return seconds / 60;
};
/* 数组求和
 * param arr: 数组
 * return 数组中数值的总和
 */
export const getArrSum = function(arr){
  return arr.reduce(function(prev, curr, idx, arr){
    if(typeof prev=="number" && typeof curr=="number"){
      return prev + curr;
    }else{
      return 0;
    }
  });
}

// 映射的时间列表
export const mapTimeList = function(startTime, endTime, group){
  // 映射的时间列表
  let timeList = []
  let diff = dayjs(endTime).diff(dayjs(startTime), group)
  for (let i = 0; i < diff; i++) {
    if(group === 'hour') timeList.push(dayjs().hour(i).format("YYYY-MM-DD HH:00:00"))
    else timeList.push(dayjs(startTime).add(i, "day").startOf("day").format("YYYY-MM-DD HH:mm:ss"))
  }
  return timeList
}