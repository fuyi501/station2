/* eslint-disable prettier/prettier */
import ajax from "./ajax";
const BASE_URL = "http://10.202.5.9:5300";

/**
 * 商超进店人次统计
 * @param {*} params
 */
export const centerCount = params => ajax(BASE_URL + "/datacenter/customer/enter", params);

/**
 * 加油区或商超区服务时间统计
 * @param {*} params
 */
export const serviceCount = params => ajax(BASE_URL + "/datacenter/service/statistic", params);

/**
 * 事件统计 strict 版本，可用来统计违规事件数
 * @param {*} params
 */
export const eventCount = params => ajax(BASE_URL + "/datacenter/statistic", params);

/**
 * 历史综合评分
 * @param {*} params
 */
export const totalScore = params => ajax(BASE_URL + "/datacenter/composite_index/history", params);

/**
 * 历史违规率统计
 * @param {*} params
 */
export const eventPercent = params => ajax(BASE_URL + "/datacenter/violate/history", params);
